api = 2
core = 7.x

; Dialog contrib module.
projects[dialog][version] = 2.0-beta1
projects[dialog][subdir] = contrib

; Editor contrib module
projects[editor][version] = 1.0-alpha7
projects[editor][subdir] = contrib

; Fences contrib module.
projects[fences][version] = 1.2
projects[fences][subdir] = contrib

; Telephone contrib module.
projects[telephone][version] = 1.0-alpha1
projects[telephone][subdir] = contrib

; CKEditor autogrow plugin library.
libraries[autogrow][download][type] = get
libraries[autogrow][download][url] = http://download.ckeditor.com/autogrow/releases/autogrow_4.6.0.zip
